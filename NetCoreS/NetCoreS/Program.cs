﻿using Library.Services;
using System;

namespace NetCoreS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Server Getway is starting");
            var server = ServerService.Instant;
            server.Listen();

            Console.WriteLine("Server Gateway is stoped. Client any key to close");
            Console.ReadKey();
        }
    }
}
