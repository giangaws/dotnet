﻿using Library.Helpers;
using Library.Models;
using System;
using System.IO;
using System.Xml;

namespace Library
{
    public class GlobalConfig
    {
        private const string FILE_CONFIG = "c.dat"; //hoang
        private static GlobalConfig _Instant;
        public static GlobalConfig Instant
        {
            get
            {
                if (_Instant == null)
                {
                    _Instant = new GlobalConfig(FILE_CONFIG);

                }
                return _Instant;
            }
        }

        public string FileName { private set; get; }
        public ConfigData Data { private set; get; }
        public bool IsReadConfigSuccess { private set; get; } = false;

        public GlobalConfig(string fileName)
        {
            FileName = fileName;
        }

        public void ReadConfig(string folder = "./")
        {
            string fileName = Path.Combine(folder, FileName);
            if (File.Exists(fileName))
            {
                try
                {
                    //string DataString = File.ReadAllText(FileName).StringDecoder();
                    //Data = JsonConvert.DeserializeObject<ConfigData>(DataString);
                    var dataString = File.ReadAllText(fileName);
                    dataString = dataString.Substring(1, dataString.Length - 1).StringDecoder();
                    //var dataString = File.ReadAllText(fileName).StringDecoder();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(dataString);
                    var root = doc.DocumentElement;
                    Data = new ConfigData()
                    {
                        SourceIp = root.GetAttribute("SourceIp"),
                        SourcePort = int.Parse(root.GetAttribute("SourcePort")),
                        DestinationIp = root.GetAttribute("DestinationIp"),
                        DestinationPort = int.Parse(root.GetAttribute("DestinationPort")),
                        CountConnectionLimit = int.Parse(root.GetAttribute("CountConnectionLimit")),
                        ProcessName = root.GetAttribute("GameClientName"),
                        Md5Hash = root.GetAttribute("Md5Hash")
                    };

                    IsReadConfigSuccess = true;
                }
                catch (Exception ex)
                {
                    File.Delete(fileName);
                    ex.StackTrace.WriteLog();
                }
            }
            else
            {
                try
                {
                    Data = new ConfigData()
                    {
                        SourceIp = "127.0.0.1",
                        SourcePort = 5001,
                        DestinationIp = "127.0.0.1",
                        DestinationPort = 5002,
                        CountConnectionLimit = 3,
                        ProcessName = "game_y",
                        Md5Hash = ""
                    };
                    Save(Data, folder);
                    IsReadConfigSuccess = true;
                }
                catch (Exception ex)
                {
                    ex.StackTrace.WriteLog();
                }
            }
        }
        public void Save(ConfigData data, string folder)
        {
            try
            {
                //string DataSave = JsonConvert.SerializeObject(data).StringEndcoder();
                //File.WriteAllText(Path.Combine(folder, FileName), DataSave);

                XmlDocument doc = new XmlDocument();
                var root = doc.CreateElement("root");
                root.SetAttribute("SourceIp", data.SourceIp.ToString());
                root.SetAttribute("SourcePort", data.SourcePort.ToString());
                root.SetAttribute("DestinationIp", data.DestinationIp.ToString());
                root.SetAttribute("DestinationPort", data.DestinationPort.ToString());
                root.SetAttribute("CountConnectionLimit", data.CountConnectionLimit.ToString());
                root.SetAttribute("GameClientName", data.ProcessName);
                root.SetAttribute("Md5Hash", data.Md5Hash);
                doc.AppendChild(root);

                var content = "a" + doc.InnerXml.StringEndcoder();
                File.WriteAllText(Path.Combine(folder, FileName), content);
            }
            catch (Exception ex)
            {
                ex.StackTrace.WriteLog();
            }
        }
    }
}
