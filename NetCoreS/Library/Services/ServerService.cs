﻿using Library.Helpers;
using Library.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Library.Services
{
    public class ServerService
    {
        public const double ClientVersion = 1.2;

        private static ServerService _Instant;
        public static ServerService Instant
        {
            get
            {
                if (_Instant == null)
                {
                    //Test
                    var config = GlobalConfig.Instant;
                    config.ReadConfig();
                    _Instant = new ServerService(config.Data);
                }
                return _Instant;
            }
        }

        public static List<ServerSocketService> ServerSockets = new List<ServerSocketService>();

        public Socket ListSocket { private set; get; }
        public ConfigData Config { private set; get; }

        public ServerService(ConfigData config)
        {
            Config = config;
        }

        public void Listen()
        {
            //Console.WriteLine($"Running with Config: {JsonConvert.SerializeObject(Config)}");

            try
            {
                if (ListSocket == null)
                {
                    //IPHostEntry host = Dns.GetHostEntry(Config.SourceIp);
                    //IPAddress ipAddress = host.AddressList[0];
                    //IPEndPoint localEndPoint = new IPEndPoint(ipAddress, Config.SourcePort);
                    IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Config.SourcePort);
                    ListSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    ListSocket.Bind(localEndPoint);
                    ListSocket.Listen(60000); //max listen

                    Console.WriteLine($"Started in port {Config.SourcePort}");
                }

                while (true)
                {
                    try
                    {
                        byte[] result = new byte[1];

                        var client = ListSocket.Accept();
                        byte[] byteCount = new byte[2];
                        Thread.Sleep(1000); //Wait 1second for receive data
                        int count = client.Receive(byteCount, 0, 2, SocketFlags.None);
                        if (count != 2)
                        {
                            result[0] = 2;
                            client.Send(result);

                            client.Disconnect(false);
                            client.Close();
                            //client.Dispose();
                            continue;
                        }

                        //int macLength = byteCount[0];
                        //byte[] macBytes = new byte[macLength];
                        //count = client.Receive(macBytes, 0, macLength, SocketFlags.None);
                        //if(count != macLength)
                        //{
                        //    client.Disconnect(false);
                        //    client.Close();
                        //    //client.Dispose();
                        //    continue;
                        //}

                        int versionLength = byteCount[0];
                        byte[] versionBytes = new byte[versionLength];
                        count = client.Receive(versionBytes, 0, versionLength, SocketFlags.None);
                        if (count != versionLength)
                        {
                            result[0] = 2;
                            client.Send(result);

                            client.Disconnect(false);
                            client.Close();
                            //client.Dispose();
                            continue;
                        }
                        string version = Encoding.ASCII.GetString(versionBytes);
                        if (double.TryParse(version, out double versionDouble))
                        {
                            if (ClientVersion > versionDouble)
                            {
                                // check wrong version
                                result[0] = 1;
                                client.Send(result);

                                client.Disconnect(false);
                                client.Close();
                                //client.Dispose();
                                continue;
                            }
                        }
                        else
                        {
                            result[0] = 2;
                            client.Send(result);

                            client.Disconnect(false);
                            client.Close();
                            //client.Dispose();
                            continue;
                        }

                        result[0] = 0;
                        client.Send(result);

                        //string mac = Encoding.ASCII.GetString(macBytes);
                        //string ip = (client.RemoteEndPoint as IPEndPoint).Address.ToString();
                        //var countConnection = ServerSockets.Count(c => c.Mac == mac && c.IpConnection == ip);
                        //if(countConnection >= Config.CountConnectionLimit)
                        //{
                        //    client.Disconnect(true);
                        //    client.Shutdown(SocketShutdown.Both);
                        //    client.Close();
                        //    client.Dispose();
                        //    continue;
                        //}

                        Console.WriteLine($"Accept Client IP: {client?.RemoteEndPoint}. Current Client Count: {(ServerSockets.Count + 1)}");

                        ServerSocketService serverSocket = new ServerSocketService(Config.DestinationIp, Config.DestinationPort, client);
                        //serverSocket.IpConnection = ip;
                        //serverSocket.Mac = mac;
                        serverSocket.Start();
                        ServerSockets.Add(serverSocket);
                    }
                    catch(Exception ex)
                    {
                        (ex.StackTrace + ": " + ex.Message).WriteLog();
                    }
                }
            }
            catch (Exception ex)
            {
                (ex.StackTrace + ": " + ex.Message).WriteLog();
            }
        }
    }
}
