﻿using Library.Helpers;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Library.Services
{
    public class ServerSocketService
    {
        private static byte[] PingPackage = new byte[1]
        {
            80
        };
        private const int TimePing = 3000;
        private const int BUFFER_SIZE = 1024;

        public string Mac { set; get; }
        public string IpConnection { set; get; }

        public string Ip { private set; get; }
        public int Port { private set; get; }

        public Socket Receive { private set; get; }
        public Socket Send { private set; get; }

        public ServerSocketService(string ip, int port, Socket client)
        {
            Ip = ip;
            Port = port;
            Receive = client;
        }

        public void Start()
        {
            if (Send == null)
            {
                //IPHostEntry host = Dns.GetHostEntry(Ip);
                //IPAddress ipAddress = host.AddressList[0];
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(Ip), Port);

                // Create a TCP/IP  socket.    
                Send = new Socket(AddressFamily.InterNetwork,
                                    SocketType.Stream, ProtocolType.Tcp);
            }

            Send.Connect(Ip, Port);

            Thread threadStartSend = new Thread(new ThreadStart(() =>
            {
                int CurrentTimePing = 0;
                try
                {
                    while (true)
                    {
                        CurrentTimePing += 100;
                        if(CurrentTimePing == TimePing)
                        {
                            // 3 byte decleare length of package
                            byte[] pingArray = new byte[4]
                            {
                            0,
                            0,
                            (byte)PingPackage.Length,
                            PingPackage[0]
                            };
                            Receive.Send(pingArray);
                            CurrentTimePing = 0;
                        }

                        if (!Receive.Connected || !Send.Connected)
                        {
                            if (Receive != null && Receive.Connected)
                            {
                                Receive.Disconnect(false);
                                Receive.Close();
                                //Receive.Dispose();
                                Receive = null;
                            }
                            if (Send != null && Send.Connected)
                            {
                                Send.Disconnect(false);
                                Send.Close();
                                //Send.Dispose();
                                Send = null;
                            }
                            ServerService.ServerSockets.Remove(this);
                            Console.WriteLine($"Remove Client. Current Client Count: {ServerService.ServerSockets.Count}");
                            break;
                        }
                        if (Receive.Available > 0)
                        {
                            CurrentTimePing = 0;
                            var bytesLength = new byte[3];
                            int countlength = 0;
                            while ((countlength += Receive.Receive(bytesLength, countlength, 3 - countlength, SocketFlags.None)) != 3) ;

                            int packageLength = bytesLength[0] * 65536 + bytesLength[1] * 256 + bytesLength[2];

                            var bytes = new byte[packageLength];
                            int count = 0;
                            while ((count += Receive.Receive(bytes, count, packageLength - count, SocketFlags.None)) != packageLength);

                            string data = Encoding.ASCII.GetString(bytes, 0, count);
                            //data.WriteLog(false);

                            // Ma hoa goi tin
                            for (int i = 0; i < count; i++)
                            {
                                bytes[i] = (byte)(bytes[i] - 2);
                            }

                            // Send to server

                            Send.Send(bytes, 0, count, SocketFlags.None);

                            Thread.Sleep(100);
                        }

                        if (Send.Available > 0)
                        {
                            CurrentTimePing = 0;
                            var bytes = new byte[BUFFER_SIZE];
                            int count = Send.Receive(bytes);

                            // giai ma goi tin
                            for (int i = 0; i < count; i++)
                            {
                                bytes[i] = (byte)(bytes[i] + 2);
                            }

                            // Send to server
                            var byteLength = new byte[3]
                            {
                                (byte)(count / 65536),
                                (byte)((count % 65536) / 256),
                                (byte)(count % 256)
                            };
                            Receive.Send(byteLength, 0, 3, SocketFlags.None);
                            Receive.Send(bytes, 0, count, SocketFlags.None);

                            Thread.Sleep(100);
                        }

                        Thread.Sleep(100);
                    }
                }
                catch (Exception ex)
                {
                    (ex.StackTrace + ": " + ex.Message).WriteLog();
                    if (Receive != null && Receive.Connected)
                    {
                        Receive.Disconnect(false);
                        Receive.Close();
                        //Receive.Dispose();
                        Receive = null;
                    }
                    if (Send != null && Send.Connected)
                    {
                        Send.Disconnect(false);
                        Send.Close();
                        //Send.Dispose();
                        Send = null;
                    }
                    ServerService.ServerSockets.Remove(this);
                    Console.WriteLine($"Remove Client. Current Client Count: {ServerService.ServerSockets.Count}");
                }
            }));
            threadStartSend.Start();
        }
    }
}
