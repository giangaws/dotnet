﻿
namespace Library.Models
{
    public class ConfigData
    {
        public string SourceIp { set; get; }
        public int SourcePort { set; get; }
        public string DestinationIp { set; get; }
        public int DestinationPort { set; get; }

        public int CountConnectionLimit { set; get; }
        public string ProcessName { set; get; }
        public string Md5Hash { set; get; }
    }
}
