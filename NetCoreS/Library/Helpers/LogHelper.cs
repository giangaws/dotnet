﻿using System;
using System.IO;

namespace Library.Helpers
{
    public static class LogHelper
    {
        public static string DIR = @".\LogFile\";
        public static string PathFileNameModel = "{0}/{1}_{2}.txt";
        public static void WriteLog(this string message, bool IsError = true)
        {
            Console.WriteLine($"Error: {message}");
            if (!Directory.Exists(DIR)) Directory.CreateDirectory(DIR);
            string FileName = string.Format(PathFileNameModel, DIR, IsError ? "error" : "info", DateTime.Now.ToString("yyyyMMdd"));
            try
            {
                FileStream fs = File.Open(FileName, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " : " + message);
                sw.Write("\r\n");
                sw.Write("----------------------------------------------------------------------------------------");
                sw.Write("\r\n");
                sw.Dispose();
                fs.Close();
                fs.Dispose();
            }
            catch{}
        }
    }
}
